package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class Storage {
	
	private static final String saveFile = "saveFile2.txt";
	
	public static void writeToFile(ArrayList<Level> levels){
		
		String txt = "";
		
		for(int i = 0; i < levels.size(); i++){
			
			if(i > 0)
				txt += '\n';
			
			Level lvl = levels.get(i);
			txt += Integer.toString(lvl.levelNo) + " " + Integer.toString(lvl.earnedCoins) + " " + Integer.toString(lvl.totalCoins);
		}
		
		FileHandle handle = Gdx.files.local(saveFile);
		handle.writeString(txt, false);
	}
	
	public static ArrayList<Level> generateList(){
		
		ArrayList<Level> levels = new ArrayList<Level>();
		
		FileHandle file = Gdx.files.local(saveFile);
		
		if(!file.exists()){
			
			return levels;
		}
		
		String txt = file.readString();
		String lvls[] = txt.split("\n");
		
		for(int i = 0; i < lvls.length; i++){
			
			String data[] = lvls[i].split(" ");
			Level l = new Level();
			l.levelNo = Integer.parseInt(data[0]);
			l.earnedCoins = Integer.parseInt(data[1]);
			l.totalCoins = Integer.parseInt(data[2]);
			
			levels.add(l);
		}
		
		return levels;
	}
	
	public static ArrayList<Level> updateList(Level cLevel, ArrayList<Level> lvls){
		
		ArrayList<Level> updatedLevels = new ArrayList<Level>();
		boolean added = false;
		
		for(int i = 0; i < lvls.size(); i++){
			
			Level l = lvls.get(i);
			if(l.levelNo == cLevel.levelNo){
				
				Level cl = bestShot(cLevel, l);
				updatedLevels.add(cl);
				added = true;
				
			}else{
				
				updatedLevels.add(l);
			}
			
		}
		
		if(!added)
			updatedLevels.add(cLevel);
		
		return updatedLevels;
	}
	
	public static Level bestShot(Level l1, Level l2){	//returns the best score of the plays of the mission
		
		if(l1.earnedCoins > l2.earnedCoins)
			return l1;
		
		return l2;
	}
	
	public static void deleteSaveFile(){
		
		FileHandle handle = Gdx.files.local(saveFile);
		handle.delete();
	}
	
}

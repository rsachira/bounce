package com.me.mygdxgame;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class Cloud {
	
	public static final int LEFT_ALIGN = 10;
	public static final int RIGHT_ALIGN = 11;
	public static final int TOP_ALIGN = 12;
	public static final int BOTTOM_ALIGN = 13;
	
	public Sprite cloudSprite;
	public float delta = 0;
	int alignment = 0;
	
	public Cloud(Sprite sprite, int align){
		
		cloudSprite = sprite;
		alignment = align;
	}
	
	public void update(){
		
		switch(alignment){
		
		case LEFT_ALIGN:
			
			cloudSprite.setPosition(WindCloud.leftBorder() - cloudSprite.getWidth() + delta, MyGame.camera.position.y - (cloudSprite.getHeight() / 2) );
			break;
			
		case RIGHT_ALIGN:
			
			cloudSprite.setPosition(WindCloud.rightBorder() + delta, MyGame.camera.position.y - (cloudSprite.getHeight() / 2) );
			break;
			
		case TOP_ALIGN:
			
			cloudSprite.setPosition(MyGame.camera.position.x - (cloudSprite.getWidth() / 2), WindCloud.topBorder() - delta);
			break;
			
		case BOTTOM_ALIGN:
			
			cloudSprite.setPosition(MyGame.camera.position.x - (cloudSprite.getWidth() / 2), WindCloud.bottomBorder() - cloudSprite.getHeight() + delta);
			break;
			
		}
	}
}

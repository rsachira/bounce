package com.me.mygdxgame;

import aurelienribon.tweenengine.TweenAccessor;

public class CloudAccessor implements TweenAccessor<Cloud> {

	@Override
	public int getValues(Cloud target, int tweenType, float[] returnValues) {
		// TODO Auto-generated method stub
		
		returnValues[0] = target.delta;
		
		return 1;
	}

	@Override
	public void setValues(Cloud target, int tweenType, float[] newValues) {
		// TODO Auto-generated method stub
		
		//target.update();
		
		//target.cloudSprite.translate(newValues[0] - target.delta, 0);
		target.delta = newValues[0];
	}

}
